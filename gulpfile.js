const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');

const path = {
    dev: {
        html: 'src/index.html',
        styles: 'src/scss/**/*.scss',
        js: 'src/js/script.js'
    },
    build: {
        root: 'build',
        styles: 'build/css',
        js: 'build/js'
    }
};

const buildHtml = () => (
    gulp.src(path.dev.html)
        .pipe(gulp.dest(path.build.root))
);

const buildJS = () => (
    gulp.src(path.dev.js)
        .pipe(gulp.dest(path.build.js))
);

const buildStyles = () => (
    gulp.src(path.dev.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(path.build.styles))
);

const serve = () => {
    gulp.watch(path.dev.html, buildHtml);
    gulp.watch(path.dev.styles, buildStyles);
    gulp.watch(path.dev.js, buildJS);

    return browserSync.init({
        server: {
            baseDir: path.build.root
        },
        files: [
            {match: path.build.root, fn: this.reload}
        ]
    })
};

gulp.task('clean', () => (
    gulp.src(path.build.root, {allowEmpty: true})
        .pipe(clean())
));

gulp.task('build', gulp.series(
    buildHtml,
    buildStyles,
    buildJS
));

gulp.task('default', gulp.series(
    'clean',
    'build',
    serve
));